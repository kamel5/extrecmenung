/*
 * See the README file for copyright information and how to reach the author.
 */

#include <langinfo.h>
#include <string>
#include <vdr/tools.h>
#include "mytools.h"

using namespace std;

string myStrReplace(string S, char C1, const char* C2)
{
  string::size_type i = 0;
  while ((i = S.find(C1, i)) != string::npos) {
     S.replace(i, 1, C2);
     i++;
     }
  return S;
}

// --- Icons ------------------------------------------------------------------
bool Icons::IsUTF8 = false;

void Icons::InitCharSet()
{
  // Taken from VDR's vdr.c
  char *CodeSet = NULL;
  if (setlocale(LC_CTYPE, ""))
     CodeSet = nl_langinfo(CODESET);
  else {
     char *LangEnv = getenv("LANG"); // last resort in case locale stuff isn't installed
     if (LangEnv) {
        CodeSet = strchr(LangEnv, '.');
        if (CodeSet)
           CodeSet++; // skip the dot
        }
     }
  if (CodeSet && strcasestr(CodeSet, "UTF-8") != 0)
     IsUTF8 = true;
}
